import { Component, OnInit, Input } from '@angular/core';
import { PanierService } from '../panier.service';

@Component({
  selector: 'app-carte-produit',
  templateUrl: './carte-produit.component.html',
  styleUrls: ['./carte-produit.component.css']
})
export class CarteProduitComponent implements OnInit {
  @Input() produit;

  constructor(
    private panier : PanierService
  ) { }

  ngOnInit() {
  }

  ajouterAuPanier(produit) {
    alert('Ajout au panier de ' + produit.designation);
    this.panier.ajouterAuPanier(produit);
  }
}
