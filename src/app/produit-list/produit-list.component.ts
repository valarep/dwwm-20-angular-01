import { Component, OnInit } from '@angular/core';
import { produits } from '../produits';

@Component({
  selector: 'app-produit-list',
  templateUrl: './produit-list.component.html',
  styleUrls: ['./produit-list.component.css']
})
export class ProduitListComponent implements OnInit {
  produits = produits;

  constructor() { }

  ngOnInit() {
  }

}
