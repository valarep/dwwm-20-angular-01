export const produits = [
    {
        ref: 'abcd1234',
        designation: 'Tournevis de la mort qui tue',
        description: 'Ceci est un magnifique tournevis',
        prix: 150,
        img: 'p85318.jpg'
    },
    {
        ref: 'bcde2345',
        designation: 'Clé Anglaise spéciale Brexit',
        prix: 49.99,
        img: 'shopping-clé-anglaise.png'
    },
    {
        ref: 'cdef3456',
        designation: 'Marteau légendaire',
        prix: 1547.23,
        img: 'marteau.jpg'
    }
];