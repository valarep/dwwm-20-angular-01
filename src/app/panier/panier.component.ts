import { Component, OnInit } from '@angular/core';
import { PanierService } from '../panier.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {
  produits;

  constructor(
    private panier : PanierService
  ) { }

  ngOnInit() {
    this.produits = this.panier.getProduits();
    console.log("this.produits");
    console.log(this.produits);
  }

}
