import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  produits = [];

  constructor() { }

  ajouterAuPanier(produit) {
    this.produits.push(produit);
    console.log(this.produits);
  }

  retirerDuPanier(produit) {
    var index = this.produits.indexOf(produit);
    this.produits.splice(index);
  }

  getProduits() {
    return this.produits;
  }

  viderPanier() {
    this.produits = [];
  }
}
